module.exports = {
    en: { code: 'GB', name: 'English' },
    fr: { code: 'FR', name: 'Français'},
    pl: { code: 'PL', name: 'Polski' },
    br: { code: 'BR', name: 'Português Brasileiro'},
    de: { code: 'DE', name: 'Deutsch'},
};
